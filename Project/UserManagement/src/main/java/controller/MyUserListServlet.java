package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.MyUserDao;
import model.MyUser;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/MyUserListServlet")
public class MyUserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MyUserListServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO 未実装：ログインセッションがない場合、ログイン画面にリダイレクトさせる

		// ユーザ一覧情報を取得
		MyUserDao userDao = new MyUserDao();
		List<MyUser> userList = userDao.findAll();

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("MyUserList", userList);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/myUserList.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO  未実装：検索処理全般

		request.setCharacterEncoding("UTF-8");

		//	String id = request.getParameter("id");
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String birthDate1 = request.getParameter("birthDate1");
		String birthDate2 = request.getParameter("birthDate2");

		MyUserDao userDao = new MyUserDao();

		//何も入力されずに検索ボタンを押した時
		if ("".equals(loginId) && "".equals(name) && "".equals(birthDate1) && "".equals(birthDate2)) {
			request.setAttribute("errMsg", "検索内容を入力してください。");
		}
		//ログインID(完全一致)
		else if ("".equals(name) && "".equals(birthDate1) && "".equals(birthDate2)) {
			List<MyUser> loginIdList = userDao.searchByloginId(loginId);
			request.setAttribute("MyUserList", loginIdList);
			request.setAttribute("loginId", loginId);
		}
		//ユーザー名(部分一致)
		else if ("".equals(loginId) && "".equals(birthDate1) && "".equals(birthDate2)) {
			List<MyUser> nameList = userDao.searchByName(name);
			request.setAttribute("MyUserList", nameList);
			request.setAttribute("name", name);
		}
		//生年月日(開始日と終了日の範囲内の日付に該当するもの)
		else if ("".equals(loginId) && "".equals(name)) {
			List<MyUser> birthDateList = userDao.searchBybirthDate(birthDate1, birthDate2);
			request.setAttribute("MyUserList", birthDateList);
			request.setAttribute("birthDate1", birthDate1);
			request.setAttribute("birthDate2", birthDate2);
		}
		//複数入力があった時
		else {
			request.setAttribute("errMsg", "１項目ごとに検索してください。");
			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthDate1", birthDate1);
			request.setAttribute("birthDate2", birthDate2);
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/myUserList.jsp");
		dispatcher.forward(request, response);

	}

}
