package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.MyUserDao;
import model.MyUser;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/MyUserDetailServlet")
public class MyUserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyUserDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		request.setCharacterEncoding("UTF-8");

		// URLからGETパラメータとしてIDを受け取る
		
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);


		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
		
		// ユーザ一覧情報を取得
				MyUserDao userDao = new MyUserDao();
				MyUser user = userDao.findById(id);

				// リクエストスコープにユーザ一覧情報をセット
				request.setAttribute("MyUser", user);


		// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/myUserDetail.jsp");
		dispatcher.forward(request, response);
		
	}


}
