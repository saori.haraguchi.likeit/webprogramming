package controller;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import controller.util.MyEncryptManager;
import dao.MyUserDao;
import model.MyUser;

/**
 * Servlet implementation class MyUpdate
 */
@WebServlet("/MyUpdateServlet")
public class MyUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MyUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// URLからGETパラメータとしてIDを受け取る

		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);

		MyUserDao userDao = new MyUserDao();
		MyUser user = userDao.findById(id);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("MyUser", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/myUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		//String createDate = request.getParameter("createDate");
		//String updateDate = request.getParameter("updateDate");

		MyUser user = new MyUser();

		// TODO idをString型からint型に変換
		int id2 = Integer.parseInt(id);

		// TODO birthDateをString型からDate型に変換
		Date date = Date.valueOf(birthDate);
		//	try {
		//String date = request.getParameter("birthDate");
		//SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
		//			Date date = sdFormat.parse(birthDate);
		//			user.setBirthDate( (java.sql.Date) date);
		//		} catch (ParseException e) {
		//		    e.printStackTrace();
		//		}

		// TODO Myuser型のuserに入力された情報を詰める
		user.setId(id2);
		user.setLoginId(loginId);
		user.setName(name);
		user.setBirthDate(date);

		if (!password.equals(password2)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			//TODO 入力内容持ち越し　何型の変数　おなじ型に変換して　request.setAttributeで
			request.setAttribute("MyUser", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/myUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}
		//		"".equals(name)	
		//		String name = null;
		//		name.equals("")

		if ("".equals(name) || "".equals(birthDate)) {
			request.setAttribute("errMsg", "必須項目を入力してください。");
			request.setAttribute("MyUser", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/myUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		MyUserDao userDao = new MyUserDao();

		if (password.equals(password2) && !"".equals(password)) {

			String pass = MyEncryptManager.encrypt(password);
			boolean user1 = userDao.passUpdate(name, birthDate, pass, id);

			if (user1 == false) {
				request.setAttribute("errMsg", "入力された内容は正しくありません。");
				request.setAttribute("MyUser", user);

				RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/myUpdate.jsp");
				dispatcher1.forward(request, response);
				return;
			}
		}

		if (password.equals(password2) && "".equals(password)) {

			boolean user2 = userDao.userUpdate(name, birthDate, id);

			if (user2 == false) {
				request.setAttribute("errMsg", "入力された内容は正しくありません。");
				request.setAttribute("MyUser", user);
				RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/myUpdate.jsp");
				dispatcher1.forward(request, response);
				return;
			}
		}

		//更新成功時
		response.sendRedirect("MyUserListServlet");
	}

}
