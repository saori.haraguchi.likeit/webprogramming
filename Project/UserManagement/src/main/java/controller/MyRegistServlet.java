package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import controller.util.MyEncryptManager;
import dao.MyUserDao;

/**
 * Servlet implementation class MyRegistServlet
 */
@WebServlet("/MyRegistServlet")
public class MyRegistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MyRegistServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/myRegist.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		MyUserDao userDao = new MyUserDao();

		//		userDao.setloginId("loginId");

		//		MyUser user = new MyUser(loginId,password, name, birthDate);
		//		request.setAttribute("myUser",user);
		//		
		//		loginId,password,password2, name, birthDate
		//		
		//		Human human = new Human();
		//		// インスタンスのフィールドに値を設定
		//		human.setName("テスト太郎");
		//		human.setAge(20);
		//		human.setBlood("A型");
		// フィールドにデータをセットしたインスタンスをスコープに追加

		if (!password.equals(password2) || "".equals(loginId) || "".equals(password) || "".equals(password2)
				|| "".equals(name) || "".equals(birthDate)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthDate", birthDate);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/myRegist.jsp");
			dispatcher.forward(request, response);
			return;
		}

		boolean user = userDao.cheakId(loginId);
		if (user == false) {
			request.setAttribute("errMsg", "入力されたログインIDを変更してください。");

			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthDate", birthDate);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/myRegist.jsp");
			dispatcher.forward(request, response);
			return;

		}

		//		boolean user2 = userDao.newUser(loginId, password, name, birthDate);

		//HttpSession session = request.getSession();
		//session.setAttribute("userInfo", userList);

		String pass = MyEncryptManager.encrypt(password);

		boolean user2 = userDao.newUser(loginId, pass, name, birthDate);

		if (user2 == false) {
			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			//TODO 入力内容持ち越し　何型の変数か　型変換はいらない　　request.setAttributeで

			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthDate", birthDate);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/myRegist.jsp");
			dispatcher.forward(request, response);
			return;
		}

		response.sendRedirect("MyUserListServlet");

	}

}
