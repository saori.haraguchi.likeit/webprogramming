<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>update</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<div class="p-3  bg-secondary text-white row">
	<div class="col-sm-12">

		<div class="text-right">
			<label> <strong>${userInfo.name} さん </strong></label>

			<style="color:#dc3545">
<
a

 

href


="
MyLogoutServlet


"
>
ログアウト


            

</
a
>
</style>

		</div>

	</div>
</div>

<br>
<br>

<h1 align="center">ユーザ情報更新</h1>

<br>
<br>
<c:if test="${errMsg != null}">
	<div class="alert alert-danger" role="alert">${errMsg}</div>
</c:if>

<form class="form-signin" action="MyUpdateServlet" method="post">

	<input type="hidden" name="loginId" value="${MyUser.loginId}">

	<div class="form-group row">
		<label for="id" class="col-3 col-form-label">ログインID</label> <label
			for="Id" class="col-9 col-form-label">${MyUser.loginId}</label>

	</div>

	<input type="hidden" name=id value="${MyUser.id}">

	<div class="form-group row">
		<label class="col-3 col-form-label">パスワード</label>
		<div class="col-9">
			<input type="password" class="form-control" name="password">
		</div>
	</div>

	<div class="form-group row">
		<label class="col-3 col-form-label">パスワード（確認）</label>
		<div class="col-9">
			<input type="password" class="form-control" name="password2">
		</div>
	</div>

	<div class="form-group row">
		<label class="col-3 col-form-label">ユーザ一名 (必須)</label>
		<div class="col-9">
			<input type="text" class="form-control" name="name"
				value="${MyUser.name}">
		</div>
	</div>

	<div class="form-group row">
		<label class="col-3 col-form-label">生年月日 (必須)</label>
		<div class="col-9">
			<input type="date" class="form-control" name="birthDate"
				value="${MyUser.birthDate}">
		</div>
	</div>

	<br> <br>

	<div class="form-group row justify-content-center">
		<button type="submit" class="btn btn-primary w-25">更新</button>
	</div>
</form>

<br>
<br>


<a href="http://localhost:8080/UserManagement/MyUserListServlet">戻る</a>



</body>
</html>