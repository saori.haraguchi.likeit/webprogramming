<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    <!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>delete</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<div class="p-3  bg-secondary text-white row">
    <div class="col-sm-12">

        <div class="text-right">
           <label> <strong>${userInfo.name} さん　　　</strong></label>
           
<style="color: #dc3545">
            <a href="MyLogoutServlet">
            ログアウト
            </a>
</style>

 </div>

    </div>
</div>

<br><br>

<h1 align="center">ユーザ削除確認</h1>

<br><br>



    <label for="id">ログインID :  ${MyUser.loginId} </label>

    <br><br>

    <label for="delete">を本当に削除してよろしいでしょうか。</label>

    <br><br>

    <div class="text-center">
    
     <form action="MyUserListServlet" >
        <button type="submit"  class="btn btn-primary w-25">
        キャンセル
        </button>
     </form>
        
        <form action= "MyDeleteServlet" method="POST" >
        <input type="hidden"  value="${MyUser.id}" name="id" >
        <button type="submit" class="btn btn-primary w-25">
        OK
        </button>
        </input>
</form>

    </div>
    


</body>

</html>