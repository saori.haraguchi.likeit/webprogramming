<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>login</title>
     <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    
</head>

<br><br>

<h1 align="center">ログイン画面</h1>

<br><br>
<br><br>

<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>



<form class="form-signin" action="MyLoginServlet" method="post">
    <div class="form-group row">
        <label for="id" class="col-2 col-form-label">ログインID</label>
        <div class="col-10">
            <input type="text" class="form-control" name="loginId" placeholder="id">
        </div>
    </div>

    <div class="form-group row">
        <label for="password" class="col-2 col-form-label">パスワード</label>
        <div class="col-10">
            <input type="password" class="form-control" name="password" placeholder="●●●●●●●●●●">
        </div>
    </div>

<br><br>

    <div class="form-group row justify-content-center">
        <button type="submit" class="btn btn-primary w-25">ログイン</button>
    </div>

</form>

</body>

</html>