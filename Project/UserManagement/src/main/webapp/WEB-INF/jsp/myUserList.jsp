<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>userList</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<body>

	<div class="p-3  bg-secondary text-white row">
		<div class="col-sm-12">

			<div class="text-right">
				<label> <strong>${userInfo.name} さん </strong></label> <a
					href="MyLogoutServlet "> ログアウト </a>

			</div>

		</div>
	</div>

	<br>
	<br>

	<h1 align="center">ユーザ一覧</h1>
	<form action="MyRegistServlet">
		<div class="text-right">
			<button type="submit" class="btn btn-primary w-25">新規登録</button>
		</div>
	</form>

<br>
	<br>
	

	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>

	<form class="form-signin" action="MyUserListServlet" method="post">

		<%-- <input type="hidden" name="Id" value="${userInfo.Id}">  --%>

		<div class="form-group row">
			<label for="id" class="col-2 col-form-label">ログインID</label>
			<div class="col-7">
				<input type="text" class="form-control" name="loginId"
					value="${loginId}">
			</div>
		</div>

		<div class="form-group row">
			<label for="user" class="col-2 col-form-label">ユーザ一名</label>
			<div class="col-7">
				<input type="text" class="form-control" name="name" value="${name}">
			</div>
		</div>

		<div class="form-group row">
			<label for="birthday" class="col-2 col-form-label">生年月日</label>
			<div class="col-3">
				<input type="date" class="form-control" name="birthDate1"
					value="${birthDate1}">
			</div>

			<p class="col-1">〜</p>

			<div class="col-3">
				<input type="date" class="form-control" name="birthDate2"
					value="${birthDate2}">
			</div>
		</div>

		<div class="text-right">
			<button type="submit" class="btn btn-primary w-25">検索</button>
		</div>

	</form>

	<hr>

	<div class="p-3 mb-2  text-white">
		<table class="table table-bordered">
			<thead class="bg-secondary">

				<tr>
					<th scope="col">ログインID</th>
					<th scope="col">ユーザ名</th>
					<th scope="col">生年月日</th>
					<th scope="col"></th>
				</tr>
			</thead>


			<tbody>
				<c:forEach var="MyUser" items="${MyUserList}">
					<tr>
						<td>${MyUser.loginId}</td>
						<td>${MyUser.name}</td>
						<td>${MyUser.birthDate}</td>
						<!-- TODO 未実装；ログインボタンの表示制御を行う -->
						<td>
							<div class="form-group row justify-content-center">
							
								<a class="btn btn-primary"
									href="MyUserDetailServlet?id=${MyUser.id}">詳細</a> 
									
									<c:if test="${userInfo.loginId == 'admin' }">
		<a	class="btn btn-success" href="MyUpdateServlet?id=${MyUser.id}">更新</a>
	</c:if>
	
	<c:if test="${userInfo.loginId == MyUser.loginId}">
		<a	class="btn btn-success" href="MyUpdateServlet?id=${MyUser.id}">更新</a>
	</c:if>
									
									<c:if test="${userInfo.loginId == 'admin' }">
		<a class="btn btn-danger" href="MyDeleteServlet?id=${MyUser.id}">削除</a>
	</c:if>
	
								
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>


		</table>
	</div>
</body>
</html>