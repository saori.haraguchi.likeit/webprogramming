<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>userDetail</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<div class="p-3  bg-secondary text-white row">
	<div class="col-sm-12">

		<div class="text-right">
			<label> <strong>${userInfo.name} さん </strong></label>

			<style="color:#dc3545">
            <a href="MyLogoutServlet">
            ログアウト
            </a>
</style>

		</div>

	</div>
</div>

<br>
<br>

<h1 align="center">ユーザ情報詳細参照</h1>

<br>
<br>

<form>

	<%-- 
		<li>ログインID : ${MyUser.loginId}</li>
		<li>ユーザ一名 : ${MyUser.name}</li>
		<li>生年月日 : ${MyUser.birthDate}</li>
		<li>登録日時 : ${MyUser.createDate}</li>
		<li>更新日時 : ${MyUser.updateDate}</li>
 --%>
		<div class="form-group row">
			<label for="user" class="col-3 col-form-label">ログインID</label> <label
				class="navbar-text"> ${MyUser.loginId}</label>
		</div>

		<div class="form-group row">
			<label for="user" class="col-3 col-form-label">ユーザ一名</label> <label
				class="navbar-text">${MyUser.name}  </label>
		</div>

		<div class="form-group row">
			<label for="birthday" class="col-3 col-form-label">生年月日</label><label
			class="navbar-text">${MyUser.birthDate}</label>
		</div>

		<div class="form-group row">
			<label for="registration" class="col-3 col-form-label">登録日時</label><label
			class="navbar-text">${MyUser.createDate}</label>
		</div>

		<div class="form-group row">
			<label for="update" class="col-3 col-form-label">更新日時</label><label
			 class="navbar-text">${MyUser.updateDate}</label>
		</div>

	

</form>


<br>
<br>

<a href="http://localhost:8080/UserManagement/MyUserListServlet">戻る</a>



</body>

</html>